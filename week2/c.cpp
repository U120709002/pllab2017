#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <limits.h>
#include <float.h>
using namespace std;

int main ( void ){

int arr[10];
bool b;
char c;
short s;
int i;
double r;

printf("The size of bool is:%ld\n",sizeof(b));
printf("The size of short is:%ld\n",sizeof(s));
printf("The size of int is:%ld\n",sizeof(i));
printf("The size of char is:%ld\n",sizeof(c));
printf("The size of double is:%ld\n",sizeof(r));
printf("The size of arr is:%ld\n",sizeof(arr));
printf("The memory address of arr[0] is:%ld\n",&arr[0]);
printf("The memory address of arr[1] is:%ld\n",&arr[1]);


return 0;
}

